<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'role_id'=>1,
        	'name'=>'Md.Admin',
        	'userName'=>'admin',
        	'email'=>'admin@gmail.com',
        	'password'=>bcrypt('rootadmin'),
        ]);

        DB::table('users')->insert([
        	'role_id'=>2,
        	'name'=>'Md.Author',
        	'userName'=>'author',
        	'email'=>'author@gmail.com',
        	'password'=>bcrypt('rootauthor'),
        ]);
    }
}
