<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/png" href="{{ asset('/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title')-{{ config('app.name', 'Laravel') }}</title>

    <!-- Font -->

    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Stylesheets -->

    <link href="{{asset('assets/frontend/css/bootstrap.css')}}" rel="stylesheet">

    <link href="{{asset('assets/frontend/css/swiper.css')}}" rel="stylesheet">

     <!-- toster massage show -->
    <link rel="stylesheet" href="{{asset('assets/global/tostr/tostr.min.css')}}">

@stack('css')

</head>
<body>
@include('layouts.frontend.partial.header')

@yield('contain')


@include('layouts.frontend.partial.footer')


    <!-- SCIPTS -->

    <script src="{{asset('assets/frontend/js/jquery-3.1.1.min.js')}}"></script>

    <script src="{{asset('assets/frontend/js/tether.min.js')}}"></script>

    <script src="{{asset('assets/frontend/js/bootstrap.js')}}"></script>

    <script src="{{asset('assets/frontend/js/swiper.js')}}"></script>

    <script src="{{asset('assets/frontend/js/scripts.js')}}"></script>

    <!-- tostr add massage -->
     <script src="{{asset('assets/global/tostr/tostr.min.js')}}"></script>
        {!! Toastr::message() !!}

         <script>
            @if($errors->any())
                @foreach($errors->all() as $error)
                    toastr.error('{{ $error }}', 'Error',{
                        closeButton:true,
                        progressBar:true,
                    });
                @endforeach
            @endif
        </script>

    @stack('js')
</body>
</html>
